<?php
/**
 * Copyright © Kowal All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MassOrdersExport\Controller\Adminhtml\Orders;

class Export extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Sales::cancel';
    /**
     * @var OrderManagementInterface|\Magento\Sales\Api\OrderManagementInterface|null
     */
    private $orderManagement;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Export constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Kowal\MassOrdersExport\Helper\Data $dataHelper
     * @param \Magento\Framework\Filesystem\Io\File $file
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Kowal\MassOrdersExport\Helper\Data $dataHelper,
        \Magento\Framework\Filesystem\Io\File $file,
        \Magento\Directory\Model\CountryFactory $countryFactory
    )
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->directoryList = $directoryList;
        $this->resultRawFactory = $resultRawFactory;
        $this->fileName = 'order_export_' . date('Ymd_His') . '.csv';
        $this->dataHelper = $dataHelper;
        $this->file = $file;
        $this->countryFactory = $countryFactory;
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection)
    {

        $this->getOrderCollection($collection);

        $this->fileFactory->create(
            $this->fileName,
            file_get_contents($this->path_to_file),

            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;
    }


    public function getOrderCollection($collection)
    {
        $orders = [];
        $headers = [];
        if (!$this->dataHelper->getGeneralCfg('hide_headers')) {
            if (!empty($this->dataHelper->getHeadersCfg('order_number'))) $headers[] = $this->dataHelper->getHeadersCfg('order_number');
            if (!empty($this->dataHelper->getHeadersCfg('order_date'))) $headers[] = $this->dataHelper->getHeadersCfg('order_date');
            if (!empty($this->dataHelper->getHeadersCfg('order_status'))) $headers[] = $this->dataHelper->getHeadersCfg('order_status');
            if (!empty($this->dataHelper->getHeadersCfg('order_purchased_from'))) $headers[] = $this->dataHelper->getHeadersCfg('order_purchased_from');
            if (!empty($this->dataHelper->getHeadersCfg('order_payment_method'))) $headers[] = $this->dataHelper->getHeadersCfg('order_payment_method');
            if (!empty($this->dataHelper->getHeadersCfg('order_shipping_method'))) $headers[] = $this->dataHelper->getHeadersCfg('order_shipping_method');
            if (!empty($this->dataHelper->getHeadersCfg('order_subtotal'))) $headers[] = $this->dataHelper->getHeadersCfg('order_subtotal');
            if (!empty($this->dataHelper->getHeadersCfg('order_tax'))) $headers[] = $this->dataHelper->getHeadersCfg('order_tax');
            if (!empty($this->dataHelper->getHeadersCfg('order_shipping'))) $headers[] = $this->dataHelper->getHeadersCfg('order_shipping');
            if (!empty($this->dataHelper->getHeadersCfg('order_discount'))) $headers[] = $this->dataHelper->getHeadersCfg('order_discount');
            if (!empty($this->dataHelper->getHeadersCfg('order_grand_total'))) $headers[] = $this->dataHelper->getHeadersCfg('order_grand_total');
            if (!empty($this->dataHelper->getHeadersCfg('order_paid'))) $headers[] = $this->dataHelper->getHeadersCfg('order_paid');
            if (!empty($this->dataHelper->getHeadersCfg('order_refunded'))) $headers[] = $this->dataHelper->getHeadersCfg('order_refunded');
            if (!empty($this->dataHelper->getHeadersCfg('order_due'))) $headers[] = $this->dataHelper->getHeadersCfg('order_due');
            if (!empty($this->dataHelper->getHeadersCfg('total_qty_items_ordered'))) $headers[] = $this->dataHelper->getHeadersCfg('total_qty_items_ordered');
            if (!empty($this->dataHelper->getHeadersCfg('customer_name'))) $headers[] = $this->dataHelper->getHeadersCfg('customer_name');
            if (!empty($this->dataHelper->getHeadersCfg('customer_id'))) $headers[] = $this->dataHelper->getHeadersCfg('customer_id');
            if (!empty($this->dataHelper->getHeadersCfg('customer_email'))) $headers[] = $this->dataHelper->getHeadersCfg('customer_email');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_name'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_name');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_company'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_company');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_street'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_street');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_zip'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_zip');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_city'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_city');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_state'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_state');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_state_name'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_state_name');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_country'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_country');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_country_name'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_country_name');
            if (!empty($this->dataHelper->getHeadersCfg('shipping_phone_number'))) $headers[] = $this->dataHelper->getHeadersCfg('shipping_phone_number');
            if (!empty($this->dataHelper->getHeadersCfg('billing_name'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_name');
            if (!empty($this->dataHelper->getHeadersCfg('billing_company'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_company');
            if (!empty($this->dataHelper->getHeadersCfg('billing_street'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_street');
            if (!empty($this->dataHelper->getHeadersCfg('billing_zip'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_zip');
            if (!empty($this->dataHelper->getHeadersCfg('billing_city'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_city');
            if (!empty($this->dataHelper->getHeadersCfg('billing_state'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_state');
            if (!empty($this->dataHelper->getHeadersCfg('billing_state_name'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_state_name');
            if (!empty($this->dataHelper->getHeadersCfg('billing_country'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_country');
            if (!empty($this->dataHelper->getHeadersCfg('billing_country_name'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_country_name');
            if (!empty($this->dataHelper->getHeadersCfg('billing_phone_number'))) $headers[] = $this->dataHelper->getHeadersCfg('billing_phone_number');
            if (!empty($this->dataHelper->getHeadersCfg('order_item_increment'))) $headers[] = $this->dataHelper->getHeadersCfg('order_item_increment');
            if (!empty($this->dataHelper->getHeadersCfg('item_name'))) $headers[] = $this->dataHelper->getHeadersCfg('item_name');
            if (!empty($this->dataHelper->getHeadersCfg('item_status'))) $headers[] = $this->dataHelper->getHeadersCfg('item_status');
            if (!empty($this->dataHelper->getHeadersCfg('item_sku'))) $headers[] = $this->dataHelper->getHeadersCfg('item_sku');
            if (!empty($this->dataHelper->getHeadersCfg('item_options'))) $headers[] = $this->dataHelper->getHeadersCfg('item_options');
            if (!empty($this->dataHelper->getHeadersCfg('item_original_price'))) $headers[] = $this->dataHelper->getHeadersCfg('item_original_price');
            if (!empty($this->dataHelper->getHeadersCfg('item_price'))) $headers[] = $this->dataHelper->getHeadersCfg('item_price');
            if (!empty($this->dataHelper->getHeadersCfg('item_qty_ordered'))) $headers[] = $this->dataHelper->getHeadersCfg('item_qty_ordered');
            if (!empty($this->dataHelper->getHeadersCfg('item_qty_invoiced'))) $headers[] = $this->dataHelper->getHeadersCfg('item_qty_invoiced');
            if (!empty($this->dataHelper->getHeadersCfg('item_qty_shipped'))) $headers[] = $this->dataHelper->getHeadersCfg('item_qty_shipped');
            if (!empty($this->dataHelper->getHeadersCfg('item_qty_canceled'))) $headers[] = $this->dataHelper->getHeadersCfg('item_qty_canceled');
            if (!empty($this->dataHelper->getHeadersCfg('item_qty_refunded'))) $headers[] = $this->dataHelper->getHeadersCfg('item_qty_refunded');
            if (!empty($this->dataHelper->getHeadersCfg('item_tax'))) $headers[] = $this->dataHelper->getHeadersCfg('item_tax');
            if (!empty($this->dataHelper->getHeadersCfg('item_discount'))) $headers[] = $this->dataHelper->getHeadersCfg('item_discount');
            if (!empty($this->dataHelper->getHeadersCfg('item_total'))) $headers[] = $this->dataHelper->getHeadersCfg('item_total');
//            if (!empty($this->dataHelper->getHeadersCfg('item_total_discout'))) $headers[] = $this->dataHelper->getHeadersCfg('item_total_discout');

            $orders[] = $headers;
        }
        $i = 1;
        foreach ($collection->getItems() as $order) {

//            file_put_contents("_order_data.txt",print_r($order->getData(),true));
//            file_put_contents("_order_shipping_data.txt",print_r($order->getShippingAddress()->getData(),true));
            $payment = $order->getPayment();
            $method = $payment->getMethodInstance();
            $paymentTitle = $method->getTitle();
            $paymentTitle = $method->getCode();


            $shippingAddress = $order->getShippingAddress();
            $person = trim($shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname());
            $company = (!empty($shippingAddress->getCompany())) ? trim($shippingAddress->getCompany()) : "";
            $street = $shippingAddress->getStreet()[0];
            $street =  (isset($shippingAddress->getStreet()[1])) ? $street . ' '. $shippingAddress->getStreet()[1] : $street;

            $billingAddress = $order->getBillingAddress();
            $blling_person = trim($billingAddress->getFirstname() . ' ' . $billingAddress->getLastname());
            $blling_company = (!empty($billingAddress->getCompany())) ? trim($billingAddress->getCompany()) : "";
            $blling_street = $billingAddress->getStreet()[0];
            $blling_street =  (isset($billingAddress->getStreet()[1])) ? $blling_street . ' '. $billingAddress->getStreet()[1] : $blling_street;
//            file_put_contents("_order_billing_data.txt",print_r($billingAddress->getData(),true));

            $orderItems = $order->getAllItems();
            $order_item_increment = 1;
            foreach ($orderItems as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }

                $cols = [];
                if (!empty($this->dataHelper->getHeadersCfg('order_number'))) $cols[$this->dataHelper->getHeadersCfg('order_number')] = $order->getIncrementId();
                if (!empty($this->dataHelper->getHeadersCfg('order_date'))) $cols[$this->dataHelper->getHeadersCfg('order_date')] = str_replace(",","",$order->getCreatedAtFormatted(2));
                if (!empty($this->dataHelper->getHeadersCfg('order_status'))) $cols[$this->dataHelper->getHeadersCfg('order_status')] = $order->getStatus();
                if (!empty($this->dataHelper->getHeadersCfg('order_purchased_from'))) $cols[$this->dataHelper->getHeadersCfg('order_purchased_from')] = preg_replace("/\r|\n/", "", $order->getStoreName());
                if (!empty($this->dataHelper->getHeadersCfg('order_payment_method'))) $cols[$this->dataHelper->getHeadersCfg('order_payment_method')] = $paymentTitle;
                if (!empty($this->dataHelper->getHeadersCfg('order_shipping_method'))) $cols[$this->dataHelper->getHeadersCfg('order_shipping_method')] = $order->getShippingMethod();
//                if (!empty($this->dataHelper->getHeadersCfg('order_subtotal'))) $cols[$this->dataHelper->getHeadersCfg('order_subtotal')] = $order->getSubtotalInclTax();
                if (!empty($this->dataHelper->getHeadersCfg('order_subtotal'))) $cols[$this->dataHelper->getHeadersCfg('order_subtotal')] = str_replace(".",",",$order->getBaseSubtotal()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_tax'))) $cols[$this->dataHelper->getHeadersCfg('order_tax')] = str_replace(".",",",$order->getTaxAmount()). 'zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_shipping'))) $cols[$this->dataHelper->getHeadersCfg('order_shipping')] = str_replace(".",",",$order->getBaseShippingInclTax()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_discount'))) $cols[$this->dataHelper->getHeadersCfg('order_discount')] = str_replace(".",",",$order->getDiscountAmount()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_grand_total'))) $cols[$this->dataHelper->getHeadersCfg('order_grand_total')] = str_replace(".",",",$order->getGrandTotal()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_paid'))) $cols[$this->dataHelper->getHeadersCfg('order_paid')] = str_replace(".",",",$order->getTotalPaid()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_refunded'))) $cols[$this->dataHelper->getHeadersCfg('order_refunded')] = str_replace(".",",",$order->getTotalRefunded()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('order_due'))) $cols[$this->dataHelper->getHeadersCfg('order_due')] = str_replace(".",",",$order->getTotalDue()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('total_qty_items_ordered'))) $cols[$this->dataHelper->getHeadersCfg('total_qty_items_ordered')] = $order->getTotalQtyOrdered();
                if (!empty($this->dataHelper->getHeadersCfg('customer_name'))) $cols[$this->dataHelper->getHeadersCfg('customer_name')] = $person;
                if (!empty($this->dataHelper->getHeadersCfg('customer_id'))) $cols[$this->dataHelper->getHeadersCfg('customer_id')] = $order->getCustomerId();
                if (!empty($this->dataHelper->getHeadersCfg('customer_email'))) $cols[$this->dataHelper->getHeadersCfg('customer_email')] = $order->getCustomerEmail();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_name'))) $cols[$this->dataHelper->getHeadersCfg('shipping_name')] = $person; // $order->getShippingDescription();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_company'))) $cols[$this->dataHelper->getHeadersCfg('shipping_company')] = $company;
                if (!empty($this->dataHelper->getHeadersCfg('shipping_street'))) $cols[$this->dataHelper->getHeadersCfg('shipping_street')] = $street;
                if (!empty($this->dataHelper->getHeadersCfg('shipping_zip'))) $cols[$this->dataHelper->getHeadersCfg('shipping_zip')] = strtoupper($shippingAddress->getPostcode());
                if (!empty($this->dataHelper->getHeadersCfg('shipping_city'))) $cols[$this->dataHelper->getHeadersCfg('shipping_city')] = $shippingAddress->getCity();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_state'))) $cols[$this->dataHelper->getHeadersCfg('shipping_state')] = $shippingAddress->getRegionId();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_state_name'))) $cols[$this->dataHelper->getHeadersCfg('shipping_state_name')] = $shippingAddress->getRegion();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_country'))) $cols[$this->dataHelper->getHeadersCfg('shipping_country')] = $shippingAddress->getCountryId();
                if (!empty($this->dataHelper->getHeadersCfg('shipping_country_name'))) $cols[$this->dataHelper->getHeadersCfg('shipping_country_name')] = ($shippingAddress->getCountryId() == 'PL') ? "Polska" : ""; //$shippingAddress->getCountryname($shippingAddress->getCountryId());
                if (!empty($this->dataHelper->getHeadersCfg('shipping_phone_number'))) $cols[$this->dataHelper->getHeadersCfg('shipping_phone_number')] = $shippingAddress->getTelephone();
                if (!empty($this->dataHelper->getHeadersCfg('billing_name'))) $cols[$this->dataHelper->getHeadersCfg('billing_name')] = $blling_person;
                if (!empty($this->dataHelper->getHeadersCfg('billing_company'))) $cols[$this->dataHelper->getHeadersCfg('billing_company')] = $blling_company;
                if (!empty($this->dataHelper->getHeadersCfg('billing_street'))) $cols[$this->dataHelper->getHeadersCfg('billing_street')] = $blling_street;
                if (!empty($this->dataHelper->getHeadersCfg('billing_zip'))) $cols[$this->dataHelper->getHeadersCfg('billing_zip')] = $billingAddress->getPostcode();
                if (!empty($this->dataHelper->getHeadersCfg('billing_city'))) $cols[$this->dataHelper->getHeadersCfg('billing_city')] = $billingAddress->getCity();
                if (!empty($this->dataHelper->getHeadersCfg('billing_state'))) $cols[$this->dataHelper->getHeadersCfg('billing_state')] = $billingAddress->getRegionId();
                if (!empty($this->dataHelper->getHeadersCfg('billing_state_name'))) $cols[$this->dataHelper->getHeadersCfg('billing_state_name')] = $billingAddress->getRegion();
                if (!empty($this->dataHelper->getHeadersCfg('billing_country'))) $cols[$this->dataHelper->getHeadersCfg('billing_country')] = $billingAddress->getCountryId();
                if (!empty($this->dataHelper->getHeadersCfg('billing_country_name'))) $cols[$this->dataHelper->getHeadersCfg('billing_country_name')] = $billingAddress->getCountryname($billingAddress->getCountryId());
                if (!empty($this->dataHelper->getHeadersCfg('billing_phone_number'))) $cols[$this->dataHelper->getHeadersCfg('billing_phone_number')] = $billingAddress->getTelephone();
                if (!empty($this->dataHelper->getHeadersCfg('order_item_increment'))) $cols[$this->dataHelper->getHeadersCfg('order_item_increment')] = $order_item_increment;
                if (!empty($this->dataHelper->getHeadersCfg('item_name'))) $cols[$this->dataHelper->getHeadersCfg('item_name')] = $item->getName();
                if (!empty($this->dataHelper->getHeadersCfg('item_status'))) $cols[$this->dataHelper->getHeadersCfg('item_status')] = $item->getStatus();
                if (!empty($this->dataHelper->getHeadersCfg('item_sku'))) $cols[$this->dataHelper->getHeadersCfg('item_sku')] = $item->getSku();
                if (!empty($this->dataHelper->getHeadersCfg('item_options'))) $cols[$this->dataHelper->getHeadersCfg('item_options')] = '';
                if (!empty($this->dataHelper->getHeadersCfg('item_original_price'))) $cols[$this->dataHelper->getHeadersCfg('item_original_price')] =  str_replace(".",",",$item->getPriceInclTax() - ($item->getDiscountAmount() / $item->getQtyOrdered())). ' zł'; // str_replace(".",",",$item->getOriginalPrice()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('item_price'))) $cols[$this->dataHelper->getHeadersCfg('item_price')] = str_replace(".",",",$item->getPriceInclTax() - ($item->getDiscountAmount() / $item->getQtyOrdered())). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_ordered'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_ordered')] = (int)$item->getQtyOrdered();
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_invoiced'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_invoiced')] = (int)$item->getQtyInvoiced();
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_shipped'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_shipped')] = (int)$item->getQtyShipped();
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_canceled'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_canceled')] = (int)$item->getQtyCanceled();
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_refunded'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_refunded')] =(int) $item->getQtyRefunded();
                if (!empty($this->dataHelper->getHeadersCfg('item_tax'))) $cols[$this->dataHelper->getHeadersCfg('item_tax')] = str_replace(".",",",$item->getTaxAmount()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('item_discount'))) $cols[$this->dataHelper->getHeadersCfg('item_discount')] = str_replace(".",",",$item->getDiscountAmount()). ' zł';
                if (!empty($this->dataHelper->getHeadersCfg('item_total'))) $cols[$this->dataHelper->getHeadersCfg('item_total')] = str_replace(".",",",$item->getRowTotalInclTax()). ' zł';
                //if (!empty($this->dataHelper->getHeadersCfg('item_total_discout'))) $cols[$this->dataHelper->getHeadersCfg('item_total_discout')] = 0; // (float)$item->getRowTotalInclTax() - (float)$item->getDiscountAmount();

                $order_item_increment++;
//                file_put_contents("_order_itme_data.txt",print_r($item->getData(),true));
                $orders[] = $cols;
            }

            if($shipping_amount = $order->getShippingAmount() && 1==2){
                if (!empty($this->dataHelper->getHeadersCfg('order_item_increment'))) $cols[$this->dataHelper->getHeadersCfg('order_item_increment')] = $order_item_increment;
                if (!empty($this->dataHelper->getHeadersCfg('item_name'))) $cols[$this->dataHelper->getHeadersCfg('item_name')] = $order->getShippingDescription();
                if (!empty($this->dataHelper->getHeadersCfg('item_status'))) $cols[$this->dataHelper->getHeadersCfg('item_status')] = '';
                if (!empty($this->dataHelper->getHeadersCfg('item_sku'))) $cols[$this->dataHelper->getHeadersCfg('item_sku')] = $order->getShippingMethod();
                if (!empty($this->dataHelper->getHeadersCfg('item_options'))) $cols[$this->dataHelper->getHeadersCfg('item_options')] = '';
                if (!empty($this->dataHelper->getHeadersCfg('item_original_price'))) $cols[$this->dataHelper->getHeadersCfg('item_original_price')] = $shipping_amount;
                if (!empty($this->dataHelper->getHeadersCfg('item_price'))) $cols[$this->dataHelper->getHeadersCfg('item_price')] = $shipping_amount - $order->getShippingDiscountAmount();
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_ordered'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_ordered')] = 1;
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_invoiced'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_invoiced')] = 1;
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_shipped'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_shipped')] = 1;
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_canceled'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_canceled')] = 0;
                if (!empty($this->dataHelper->getHeadersCfg('item_qty_refunded'))) $cols[$this->dataHelper->getHeadersCfg('item_qty_refunded')] = 0;
                if (!empty($this->dataHelper->getHeadersCfg('item_tax'))) $cols[$this->dataHelper->getHeadersCfg('item_tax')] = $order->getShippingTaxAmount();
                if (!empty($this->dataHelper->getHeadersCfg('item_discount'))) $cols[$this->dataHelper->getHeadersCfg('item_discount')] = $order->getShippingDiscountAmount();
                if (!empty($this->dataHelper->getHeadersCfg('item_total'))) $cols[$this->dataHelper->getHeadersCfg('item_total')] = $shipping_amount;
                //if (!empty($this->dataHelper->getHeadersCfg('item_total_discout'))) $cols[$this->dataHelper->getHeadersCfg('item_total_discout')] = $shipping_amount - $order->getShippingDiscountAmount();
                $orders[] = $cols;
            }




            if ($this->dataHelper->getGeneralCfg('explode_orders_files')) {
                $this->getFileName($i)->saveArrayToCsv($orders);
                $orders = [];
                $orders[] = $headers;
                $i++;
            }
        }

        $this->getFileName('a')->saveArrayToCsv($orders);
    }

    private function getFileName($i)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders', 0775);
        }
        $this->path_to_file = $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'order_export_' . date('Ymd_His') . '_' . $i . '.csv';
        return $this;
    }

    private function saveArrayToCsv($array)
    {
        $fp = fopen($this->path_to_file, 'w');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    public function getCountryname($countryCode){
        $country = $this->countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
}

