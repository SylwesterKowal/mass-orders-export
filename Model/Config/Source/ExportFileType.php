<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MassOrdersExport\Model\Config\Source;

class ExportFileType implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'csv', 'label' => __('csv')],['value' => 'xml', 'label' => __('xml')]];
    }

    public function toArray()
    {
        return ['csv' => __('csv'),'xml' => __('xml')];
    }
}
