<?php
namespace Kowal\MassOrdersExport\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const SECTIONS      = 'mass_orders_export';   // module name
    const GROUPS        = 'settings';        // setup general
    const HGROUPS        = 'headers';        // setup general

    public function getConfig($cfg=null)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function getGeneralCfg($cfg=null) 
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS.'/'.self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if(isset($config[$cfg])) return $config[$cfg];
        return $config;
    }

    public function getHeadersCfg($cfg=null)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS.'/'.self::HGROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if(isset($config[$cfg])) return $config[$cfg];
        return $config;
    }


}
