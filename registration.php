<?php
/**
 * Copyright © Kowal All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Kowal_MassOrdersExport', __DIR__);

